const path ="http://120.76.136.48:5000/"
/**
 * promise化接口
 */
function wxRequest(url, method, params) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: path + url,
      method: method,
      data: params,
      header: {
        'content-type': 'application/json'
      },
      success: res => resolve(res),
      fail: res => reject(res)
    })
  });
}	

function request(url,type,params = {}) {
  return wxRequest(url, type, params);
}
// 导出
module.exports = {
  request
}