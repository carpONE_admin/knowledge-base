// pages/navbar/navbar.js
Component({
	data: {
    active: 0,
    list: [
      {
        name: 'home',
        icon: "home-o",
        text: "首页",
        url: "/pages/index/index"
      },
      {
        name: 'detail',
        icon: "bar-chart-o",
        text: "详情",
        url: "/pages/detail/detail"
      },
      {
        name: 'setting',
        icon: "apps-o",
        text: "管理",
        url: "/pages/setting/setting"
      },
      {
        name: 'my',
        icon: "manager-o",
        text: "我的",
        url: "/pages/my/my"
      }]
  },
	methods:{
		onChange(event) {
      this.setData({ active: event.detail });
     // console.log(event.detail)
      wx.switchTab({
        url: this.data.list[event.detail].url
      });
    },
		init() {
      const page = getCurrentPages().pop();
      this.setData({
        active: this.data.list.findIndex(item => item.url === `/${page.route}`)
      });
    }
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage() {

	}
})