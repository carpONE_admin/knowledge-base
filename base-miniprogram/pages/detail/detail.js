import * as echarts from '../ec-canvas/echarts.js';
import { request } from "../../util/request"
Page({
	data: {
		incometotal: 200,
		paytotal: 300,
		loading: false,
		ec: {
			lazyLoad: true
		},
		house: {
			lazyLoad: true
		},
		expenseType: { lazyLoad: true },
		timer: '',//实时刷新，所以设置了个定时器
		categoryListData: [],//类别数据
	},
	refresh() {
		this.loading = true
		this.getHeaderOption();
		this.getHouseOption();
	},
	async onLoad() {
		setTimeout(() => {
			this.loading = true
			this.getHeaderOption();
			this.getHouseOption();
		}, 1000);
	},
	onReady() {
		this.component = this.selectComponent("#mychart-dom-pie");
		this.houseComponent = this.selectComponent("#houstChart");
		this.expenseTypeComponent = this.selectComponent("#expenseTypeChart");
	},
	onUnload: function () {
		// 清除了定时器
		clearInterval(this.data.timer)
	},
	// 给图表加上数据
	getHeaderOption: function () {
		const that = this;
		let expense = 0;
		request("rmb/getForMonth?type=expense&time1=&time2=", "get").then(({ data }) => {
			let wechat = 0;
			let alipay = 0;
			let flowers = 0;
			let bankCard = 0;
			data.message.forEach((item) => {
				expense += item.amount;
				if (item.payType === "0") {
					wechat += item.amount;
				} else if (item.payType === "1") {
					alipay += item.amount;
				} else if (item.payType === "2") {
					flowers += item.amount;
				} else {
					bankCard += item.amount;
				}
			});
			that.init_expenseEchart(expense)
			that.init_expenseTypeChart(wechat, alipay, flowers, bankCard)
		})
	},
	getHouseOption: function () {
		const that = this;
		request("house/all", "get").then(({ data }) => {
			let waterCount = data.message.water.count;
			let electricityCount = data.message.electricity.count;
			let wLast = data.message.wLast.count;
			let eLast = data.message.eLast.count;
			that.init_houseChart(waterCount, electricityCount, wLast, wLast)
			that.loading = false
		})
	},

	// 支出系列图表
	init_expenseEchart: function (expense) {
		this.component.init((canvas, width, height) => {
			const chart = echarts.init(canvas, null, {
				width: width,
				height: height
			});
			setHeaderOption(chart, expense)
			this.chart = chart;
			return chart;
		});
	},
	//房租系列图表
	init_houseChart: function (waterCount, electricityCount, wLast, eLast) {
		this.houseComponent.init((canvas, width, height) => {
			const chart = echarts.init(canvas, null, {
				width: width,
				height: height
			});
			setHouseOption(chart, waterCount, electricityCount, wLast, eLast)
			this.chart = chart;
			return chart;
		});
	},
	//支出类型系列图表
	init_expenseTypeChart: function (wechat, alipay, flowers, bankCard) {
		this.expenseTypeComponent.init((canvas, width, height) => {
			const chart = echarts.init(canvas, null, {
				width: width,
				height: height
			});
			setExpenseTypeOpiton(chart, wechat, alipay, flowers, bankCard)
			this.chart = chart;
			return chart;
		});
	},
	onShow() {
		//初始化底部栏
		this.getTabBar().init();
	},
})

function setHeaderOption(chart, expense) {
	var option = {
		tooltip: { formatter: "剩余 : {c}%" },
		title: {
			text:
				"总量为：4000；支出：" +
				parseInt(expense) +
				" 剩余：" +
				(4000 - parseInt(expense)),
		},
		series: [
			{
				type: "gauge",
				axisLine: {
					lineStyle: {
						width: 30,
						color: [
							[0.3, "red"],
							[0.7, "blue"],
							[1, "green"],
						],
					},
				},
				pointer: {
					itemStyle: {
						color: "auto",
					},
				},
				axisTick: {
					distance: -30,
					length: 5,
					lineStyle: {
						color: "#fff",
						width: 2,
					},
				},
				splitLine: {
					distance: -30,
					length: 30,
					lineStyle: {
						color: "#fff",
						width: 4,
					},
				},
				axisLabel: {
					color: "auto",
					distance: 40,
					fontSize: 12,
				},
				detail: {
					valueAnimation: true,
					formatter: "{value} %",
					color: "auto",
					fontSize: 12,
				},
				data: [{
					name: "剩余",
					value: parseInt(4000 - expense) / 40,
				}],
			},
		],
	}
	chart.setOption(option)
}

function setHouseOption(chart, waterCount, electricityCount, wLast, eLast) {
	const option = {
		tooltip: {},
		legend: {
			orient: "vertical",
			left: "left",
		},
		series: [
			{
				name: "支出列表",
				type: "pie",
				radius: [0, "50%"],
				data: [
					{
						value: (waterCount - wLast) * 5,
						name: "水费",
						itemStyle: { color: "blue" },
					},
					{ value: (electricityCount - eLast) * 1.5, name: "电费" },
					{ value: waterCount, name: "水量(方)" },
					{ value: electricityCount, name: "电量(度)" },
				]
			},
		],
	};
	chart.setOption(option)
}

function setExpenseTypeOpiton(chart, wechat, alipay, flowers, bankCard) {
	const optionT = {
		tooltip: {},
		xAxis: {
			type: "category",
			data: ["微信", "支付宝", "花呗", "银行卡"],
		},
		yAxis: {
			type: "value",
		},
		series: [
			{
				data: [wechat, alipay, flowers, bankCard],
				type: "bar",
				showBackground: true,
				backgroundStyle: {
					color: "rgba(180, 180, 180, 0.2)",
				},
			},
		],
	};
	chart.setOption(optionT)
}