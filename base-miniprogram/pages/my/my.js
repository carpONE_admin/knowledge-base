import { request } from "../../util/request"
Page({
	data: {
		formatter(day) {
		},
	},
	onLoad() {
		let that = this;
		request("rmb/sum", "get").then(({ data }) => {
			that.setData({
				formatter: function (day) {
					const month = day.date.getMonth() + 1 < 10 ? "0" + (day.date.getMonth() + 1) : day.date.getMonth() + 1;
					const date = day.date.getDate() < 10 ? "0" + day.date.getDate() : day.date.getDate();
					let dateDay = (day.date.getFullYear() + "-" + month + "-" + date);
					data.message.forEach(item => {
						if (item.outTime.split(" ")[0] == dateDay) {
							day.topInfo = item.sumMoney
						}
					});
					return day;
				}
			})
		})
	},
	onShow() {
		//初始化底部栏
		this.getTabBar().init();
	},
})