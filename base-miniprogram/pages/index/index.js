Page({
	data: {
		current: 1,
		imageList: [
			{
				url: 'https://activity.vtuzx.com/doc/vtuUI/weapp/swiper/1.png',
				mode: "widthFix"
			},
			{
				url: 'https://activity.vtuzx.com/doc/vtuUI/weapp/swiper/2.png',
				mode: "widthFix"
			},
			{
				url: 'https://activity.vtuzx.com/doc/vtuUI/weapp/swiper/3.png',
				mode: "widthFix"
			},
			{
				url: 'https://activity.vtuzx.com/doc/vtuUI/weapp/swiper/4.png',
				mode: "widthFix"
			},
			{
				url: 'https://activity.vtuzx.com/doc/vtuUI/weapp/swiper/5.png',
				mode: "widthFix"
			}
		],
		tabList: [],
		tabListTwo: [],
		tabsValue: "623acf7b639ab1b33452963e",
		content: "https://ui.vtuzx.com/#/plug/tabbar"
	},
	/* 	onLoad() {
			var that = this;
			wx.showLoading({
				title: '加载中...',
			})
			wx.request({
				url: 'https://hello-cloudbase-3gw8f5h0ac31340e-1259504677.ap-guangzhou.app.tcloudbase.com/container-springboot-base/menuitem',
				success({ data }) {
					var list = []
					for (var i = 0; i < data.message.length; i++) {
						if (!data.message[i].isSubMenu) {
							list.push(data.message[i])
						}
					};
					that.setData({
						tabList: list
					})
	
				}
			})
	
			wx.request({
				url: 'https://hello-cloudbase-3gw8f5h0ac31340e-1259504677.ap-guangzhou.app.tcloudbase.com/container-springboot-base/content/queryId?id=' + this.data.tabsValue,
				success({ data }) {
					that.setData({
						content: data.message.contentDetail
					})
				}
			})
	
			wx.request({
				url: 'https://hello-cloudbase-3gw8f5h0ac31340e-1259504677.ap-guangzhou.app.tcloudbase.com/container-springboot-base/menuitemGroupItem',
				success({ data }) {
					that.setData({
						tabListTwo: data.message
					})
					wx.hideLoading({
						success: (res) => {
							wx.showToast({
								title: '加载完毕',
							})
						},
					})
				}
			})
		}, */

	swiperChange({ detail }) {
		this.setData({
			current: detail.current
		})
	},
	tabsChange({ detail }) {
		var that = this;
		this.setData({
			tabsValue: detail.name
		});
		wx.request({
			url: 'https://hello-cloudbase-3gw8f5h0ac31340e-1259504677.ap-guangzhou.app.tcloudbase.com/container-springboot-base/content/queryId?id=' + detail.name,
			success({ data }) {
				that.setData({
					content: data.message.contentDetail
				})
			}
		})
	},
	wxmlTagATap({ detail }) {
		wx.setClipboardData({
			data: detail.src,
			success() {
				wx.showToast({
					title: "复制成功"
				})
			}
		})

	},
	onShow() {
		this.getTabBar().init();
	},
})
