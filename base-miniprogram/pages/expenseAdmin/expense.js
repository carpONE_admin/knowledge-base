import Toast from "../../miniprogram_npm/vtuweapp/toast/vtu-index"
import { request } from "../../util/request"
Page({
	data: {
		show: false,
		timeShow: false,
		rmbList: [],
		title: 0,
		time: "",
		formData: {
			title: "早餐",
			amount: 0,
			payType: 0,
			outTime: "",
			incident: "",
			type: "expense",
		},
		titleOptions: [
			{ type: 0, label: "早餐" },
			{ type: 1, label: "午餐" },
			{ type: 2, label: "晚餐" },
			{ type: 3, label: "出行" },
			{ type: 4, label: "房租" },
			{ type: 5, label: "其他" },
		],
		options: [
			{
				type: 0,
				label: "微信",
			},
			{
				type: 1,
				label: "支付宝",
			},
			{
				type: 2,
				label: "花呗",
			},
			{
				type: 3,
				label: "银行卡",
			},
		]
	},

	onLoad(options) {
		this.getData()
	},

	onReady() {

	},
	onShow() {

	},
	getData() {
		const that = this;
		request("rmb/getForMonth?type=expense&time1=&time2=", "get").then(({ data }) => {
			that.setData({
				rmbList: data.message
			})
		})
	},
	add() {
		this.setData({
			show: true
		})
	},
	onClose() {
		this.setData({ show: false });
	},
	onSelectTitle(event) {
		const { value } = event.detail;
		let data = (this.data.titleOptions[value]);
		this.setData({
			title: value,
			'formData.title': data.label
		})
	},
	onSelectPayType(event) {
		const { value } = event.detail;
		this.setData({
			'formData.payType': value
		})
	},
	pickerDateChange(e) {
		const { value } = e.detail;
		this.setData({
			timeShow: true,
			'formData.outTime': value
		})
	},
	pickerTimeChange(e) {
		const { value } = e.detail;
		const out = this.data.formData.outTime;
		this.setData({
			'formData.outTime': out + " " + value,
			time: value
		})
		console.log(this.data.formData.outTime);
	},
	onAmount(e) {
		const { value } = e.detail;
		this.setData({
			'formData.amount': value,
		})
	},
	onTextarea(e) {
		const { value } = e.detail;
		this.setData({
			'formData.incident': value,
		})
	},
	confirm() {
		let that = this;
		request("rmb/add", "post", this.data.formData).then(({ data }) => {
			console.log(data);
			if (data.error == 0) {
				that.setData({
					show: false
				})
				that.getData()
				Toast('添加成功');
			} else {
				Toast().error(data.message);
			}
		})
	}
})