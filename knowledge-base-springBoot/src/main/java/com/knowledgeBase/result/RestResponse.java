package com.knowledgeBase.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author 鲤鱼乡
 * date 2021年07月09日9:52
 * info RestResponse
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RestResponse extends HashMap<String, Object> {


    /**
     * 禁止通过构造函数构造对象，只能通过静态方法获取实例。
     *
     * @see #ok()
     * @see #ok(String)
     * @see #fail()
     * @see #fail(String)
     */
    private RestResponse() {
    }

    /**
     * 接口执行成功的返回数据，其中属性 error = 0
     */
    public static RestResponse ok() {
        RestResponse result = new RestResponse();
        result.put(AppConstant.ERROR, AppConstant.OK);
        return result;
    }

    /**
     * 接口执行成功的返回数据，并设置文本消息
     * <p>
     * param msg
     */
    public static RestResponse ok(String msg) {
        RestResponse result = new RestResponse();
        Objects.requireNonNull(result.put(AppConstant.ERROR, AppConstant.OK)).msg(msg);
        return result;
    }

    /**
     * 接口执行成功的返回数据，并设置对象数据
     * <p>
     * param item
     */
    public static RestResponse ok(Object item) {
        RestResponse result = new RestResponse();
        Objects.requireNonNull(result.put(AppConstant.ERROR, AppConstant.OK)).data(item);
        return result;
    }

    /**
     * 接口执行成功的返回数据，并设置列表对象数据
     * param list
     */
    public static RestResponse ok(List<?> list) {
        RestResponse result = new RestResponse();
        Objects.requireNonNull(result.put(AppConstant.ERROR, AppConstant.OK)).list(list);
        return result;
    }


    /**
     * 接口执行失败的返回数据，其中属性 error = 1
     */
    public static RestResponse fail() {
        RestResponse result = new RestResponse();
        result.put(AppConstant.ERROR, AppConstant.FAIL);
        return result;
    }

    /**
     * 接口执行失败的返回数据，并设置文本消息，其中属性 error = 1, message = {msg}
     * param msg
     */
    public static RestResponse fail(String msg) {
        RestResponse result = new RestResponse();
        Objects.requireNonNull(result.put(AppConstant.ERROR, AppConstant.FAIL)).msg(msg);
        return result;
    }

    /**
     * 接口执行失败的返回数据，自定义状态码，其中属性 error = {errorCode}
     * param errorCode
     */
    public static RestResponse fail(int errorCode) {
        RestResponse result = new RestResponse();
        result.put(AppConstant.ERROR, errorCode);
        return result;
    }


    /**
     * 设置接口返回的文本消息，属性 key: message
     * param msg
     */
    public RestResponse msg(String msg) {
        this.put(AppConstant.MESSAGE, msg);
        return this;
    }

    /**
     * 设置接口返回的数据对象，属性 key: item
     * param item
     */
    public RestResponse data(Object item) {
        this.put(AppConstant.MESSAGE, item);
        return this;
    }

    /**
     * 设置接口返回的数据对象列表，属性 key: list
     * param list
     */
    public RestResponse list(List<?> list) {
        this.put(AppConstant.MESSAGE, list);
        return this;
    }

    /**
     * 设置接口返回的数据项，并指定数据项的属性 key
     * param key
     * param value
     */
    public RestResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }


}
    