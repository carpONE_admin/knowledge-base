package com.knowledgeBase.result;

/**
 * @author 鲤鱼乡
 * date 2022年03月16日下午 7:55
 * info AppConstant
 */
public class AppConstant {


    // 统一返回数据
    public static final String MESSAGE = "message";


    // 状态码
    public static final String ERROR = "error";

    // 代表执行成功
    public static int OK = 0;

    // 代表执行失败
    public static int FAIL = 1;

    // 代表服务器运行异常
    public static int RunTime = 2;

    // 代表空指针异常
    public static int NullPointer = 3;

    // Assert异常
    public static int Assert = 4;

    // 实体异常
    public static int Entity = 5;

    // 未知方法异常
    public static int NoSuchMethod = 6;


    // 400错误
    public static int HttpMessageNotReadable = 8;

    // 400错误
    public static int TypeMismatch = 9;

    // 400错误
    public static int MissingServletRequestParameter = 10;

    // 405错误
    public static int HttpRequestMethodNotSupported = 11;

    // 406错误
    public static int HttpMediaTypeNotAcceptable = 12;

    // 500错误
    public static int Run500 = 13;

    // 栈溢出
    public static int StackOverflow = 14;


    // 其他异常
    public static int other = 15;

}
