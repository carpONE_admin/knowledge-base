package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.Rmb;
import com.knowledgeBase.entity.RmbVo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-05
 */
@Repository
public interface RmbMapper extends BaseMapper<Rmb> {

    @Select("select * from t_rmb where  type =#{type} and outTime >=#{timeOne} and outTime <=#{timeTwo}  order by outTime desc")
    List<Rmb> getTimePeriod(String type, String timeOne, String timeTwo);

    @Select("select * from t_rmb where DATE_FORMAT( outTime, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) and type =#{type} order by outTime desc")
    List<Rmb> getForType(String type);

    @Select("SELECT * FROM t_rmb WHERE DATE_FORMAT( outTime, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) and type='expense'  order by outTime desc")
    List<Rmb> getThisMonth();

    @Select("select sum(amount) as sumMoney,outTime from t_rmb group by DATE(outTime)")
    List<RmbVo> getMoneySumForDate();
}
