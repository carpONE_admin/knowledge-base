package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.MenuitemGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Service
public interface MenuitemgroupMapper extends BaseMapper<MenuitemGroup> {
    @Select("select * from t_menuitemgroup")
    List<MenuitemGroup> selectAll();

    @Select("select * from t_menuitemgroup where subMenuId = #{id}")
    List<MenuitemGroup> queryByMenuItemId(String id);

    @Select("select * from t_menuitemgroup where MenuItemGroupId = #{MenuItemGroupId}")
    List<MenuitemGroup> queryById(String MenuItemGroupId);

    @Insert("insert into t_menuitemgroup(MenuItemGroupId,subMenuId,MenuItemGroupTitle,MenuItemGroupIcon,createdTime,subMenuTitle) " +
            "values(#{MenuItemGroupId},#{subMenuId},#{MenuItemGroupTitle},#{MenuItemGroupIcon},#{createdTime},#{subMenuTitle})")
    boolean addMenuitemgroup(String MenuItemGroupId, String subMenuId, String MenuItemGroupTitle, String MenuItemGroupIcon, String createdTime, String subMenuTitle);

    @Update("update t_menuitemgroup set subMenuId=#{subMenuId} , MenuItemGroupTitle=#{MenuItemGroupTitle}," +
            "MenuItemGroupIcon =#{MenuItemGroupIcon},subMenuTitle=#{subMenuTitle} where MenuItemGroupId = #{MenuItemGroupId}")
    boolean updateMenuItemGroup(String MenuItemGroupId, String subMenuId, String MenuItemGroupTitle, String MenuItemGroupIcon, String subMenuTitle);

    @Delete("delete from t_menuitemgroup where  MenuItemGroupId = #{MenuItemGroupId}")
    boolean deleteById(String MenuItemGroupId);
}
