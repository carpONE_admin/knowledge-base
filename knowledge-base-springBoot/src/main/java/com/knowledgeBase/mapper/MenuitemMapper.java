package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.Menuitem;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Service
public interface MenuitemMapper extends BaseMapper<Menuitem> {

    @Select("select * from t_menuitem")
    List<Menuitem> selectAll();

    @Select("select * from t_menuitem where menuItemId = #{menuItemId}")
    List<Menuitem> queryById(String menuItemId);

    @Insert("insert into t_menuitem(menuItemId,icon,menuTitle,isSubMenu,hasMenuItemGroup,createdTime) " +
            "values(#{menuItemId},#{icon},#{menuTitle},#{isSubMenu},#{hasMenuItemGroup},#{createdTime})")
    boolean addMenuItem(String menuItemId, String icon, String menuTitle, boolean isSubMenu,
                        boolean hasMenuItemGroup, String createdTime);

    @Update("update t_menuitem set icon=#{icon},menuTitle =#{menuTitle},isSubMenu =#{isSubMenu}," +
            "hasMenuItemGroup =#{hasMenuItemGroup} where  menuItemId = #{menuItemId}")
    boolean updateMenuItem(String menuItemId, String icon, String menuTitle, boolean isSubMenu,
                           boolean hasMenuItemGroup);


    @Delete("delete from t_menuitem where  menuItemId = #{menuItemId}")
    boolean deleteById(String menuItemId);
}
