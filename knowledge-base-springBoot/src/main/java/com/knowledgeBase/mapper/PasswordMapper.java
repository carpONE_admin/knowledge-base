package com.knowledgeBase.mapper;

import com.knowledgeBase.entity.Password;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 是否管理员 Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-23
 */
@Repository
public interface PasswordMapper extends BaseMapper<Password> {

    @Update("update t_password set times=#{count} where id =#{id}")
    boolean updateForTimes(Integer count,Integer id);

}
