package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.House;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-11
 */
@Service
public interface HouseMapper extends BaseMapper<House> {

    @Select("select * from t_house where type =#{type} and day >=#{startTime} or day <=#{endTime}")
    List<House> getForTime(boolean type, String startTime, String endTime);

    @Select("select * from t_house where type =#{type}")
    List<House> getForType(boolean type);

    @Select("select * from t_house where DATE_FORMAT( day, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) and type =#{type}  order by day desc")
    List<House> getOrder(boolean type);
}
