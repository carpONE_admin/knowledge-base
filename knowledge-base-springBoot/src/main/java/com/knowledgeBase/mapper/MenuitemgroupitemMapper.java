package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.MenuitemGroupItem;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Service
public interface MenuitemgroupitemMapper extends BaseMapper<MenuitemGroupItem> {
    @Select("select * from t_menuitemgroupitem")
    List<MenuitemGroupItem> selectAll();

    @Select("select * from t_menuitemgroupitem where MenuItemGroupItemId = #{MenuItemGroupItemId}")
    List<MenuitemGroupItem> queryById(String MenuItemGroupItemId);

    @Insert("insert into t_menuitemgroupitem(MenuItemGroupItemId,MenuItemGroupId,MenuItemGroupItemTitle,MenuItemGroupItemIcon,createdTime,menuItemGroupTitle) " +
            "values(#{MenuItemGroupItemId},#{MenuItemGroupId},#{MenuItemGroupItemTitle},#{MenuItemGroupItemIcon},#{createdTime},#{menuItemGroupTitle})")
    boolean addMenuitemgroupItem(String MenuItemGroupItemId, String MenuItemGroupId, String MenuItemGroupItemTitle,
                                 String MenuItemGroupItemIcon, String createdTime, String menuItemGroupTitle);

    @Update("update t_menuitemgroupitem set MenuItemGroupId=#{MenuItemGroupId}," +
            "MenuItemGroupItemTitle=#{MenuItemGroupItemTitle},MenuItemGroupItemIcon=#{MenuItemGroupItemIcon},menuItemGroupTitle=#{menuItemGroupTitle}" +
            "where MenuItemGroupItemId = #{MenuItemGroupItemId}")
    boolean updateMenuitemgroupItem(String MenuItemGroupId, String MenuItemGroupItemTitle, String MenuItemGroupItemIcon, String menuItemGroupTitle,String MenuItemGroupItemId);

    @Delete("delete from t_menuitemgroupitem where  MenuItemGroupItemId = #{MenuItemGroupItemId}")
    boolean deleteById(String MenuItemGroupItemId);
}
