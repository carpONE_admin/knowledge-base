package com.knowledgeBase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.knowledgeBase.entity.Content;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Repository
public interface ContentMapper extends BaseMapper<Content> {

    @Select("select * from t_content")
    List<Content> selectAll();

    @Select("select * from t_content where contentId = #{contentId}")
    List<Content> selectByContentId(String contentId);

    @Select("select * from t_content where menuItemId = #{menuItemId}")
    Content selectByMenuItemId(String menuItemId);

    @Select("select * from t_content where menuItemId = #{menuItemId} and contentId=#{contentId}")
    List<Content> selectByMenuItemId_ContentId(String menuItemId, String contentId);


    @Insert("insert into t_content(contentId, menuItemId, contentDetail, whereFrom, author, reference, projectAddress, createdTime,menuItemTitle)" +
            "values (#{contentId}, #{menuItemId}, #{contentDetail}, #{whereFrom}, #{author}, #{reference}, #{projectAddress}, #{createdTime},#{menuTitle})")
    boolean addContent(String contentId, String menuItemId, String contentDetail,
                       String whereFrom, String author, String reference,
                       String projectAddress, String createdTime,String menuTitle);

    @Delete("delete from t_content where  contentId = #{contentId}")
    boolean deleteContent(String contentId);

    @Update("update t_content set  menuItemId =#{menuItemId},contentDetail=#{contentDetail}," +
            "whereFrom=#{whereFrom},author=#{author},reference=#{reference}," +
            "projectAddress=#{projectAddress},updateTime=#{updateTime},menuItemTitle=#{menuItemTitle} where  contentId =#{contentId}")
    boolean updateContent(String contentId, String menuItemId, String contentDetail,
                          String whereFrom, String author, String reference,
                          String projectAddress, String updateTime,String menuItemTitle);

    @Select("select * from t_content where contentDetail like concat('%',#{title},'%') " +
            "or whereFrom like concat('%',#{title},'%')  or author like concat('%',#{title},'%') or" +
            " reference like  concat('%',#{title},'%')  or projectAddress like concat('%',#{title},'%') ")
    List<Content> fuzzyQuery(String title);
}
