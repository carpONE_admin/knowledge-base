package com.knowledgeBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnowledgeBaseSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowledgeBaseSpringBootApplication.class, args);
    }

}
