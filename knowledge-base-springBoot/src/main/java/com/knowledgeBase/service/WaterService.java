package com.knowledgeBase.service;

import com.knowledgeBase.entity.House;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-11
 */
public interface WaterService extends IService<House> {

}
