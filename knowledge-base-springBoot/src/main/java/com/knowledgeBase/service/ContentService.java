package com.knowledgeBase.service;

import com.knowledgeBase.entity.Content;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
public interface ContentService extends IService<Content> {

}
