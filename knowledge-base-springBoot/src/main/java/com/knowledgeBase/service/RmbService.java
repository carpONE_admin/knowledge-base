package com.knowledgeBase.service;

import com.knowledgeBase.entity.Rmb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-05
 */
public interface RmbService extends IService<Rmb> {

}
