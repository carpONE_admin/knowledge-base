package com.knowledgeBase.service;


import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.io.Serializable;

public interface IOssService {
    Serializable upload(String url);

    Serializable upload(MultipartFile file);

    String upload(InputStream inputStream, String name);

    String getEndPoint();
}
