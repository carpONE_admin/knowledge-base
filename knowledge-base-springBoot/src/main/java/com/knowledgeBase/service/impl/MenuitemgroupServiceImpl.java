package com.knowledgeBase.service.impl;

import com.knowledgeBase.entity.MenuitemGroup;
import com.knowledgeBase.mapper.MenuitemgroupMapper;
import com.knowledgeBase.service.MenuitemgroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Service
public class MenuitemgroupServiceImpl extends ServiceImpl<MenuitemgroupMapper, MenuitemGroup> implements MenuitemgroupService {

}
