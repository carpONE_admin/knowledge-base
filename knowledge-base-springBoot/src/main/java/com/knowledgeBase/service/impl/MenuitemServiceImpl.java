package com.knowledgeBase.service.impl;

import com.knowledgeBase.entity.Menuitem;
import com.knowledgeBase.mapper.MenuitemMapper;
import com.knowledgeBase.service.MenuitemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Service
public class MenuitemServiceImpl extends ServiceImpl<MenuitemMapper, Menuitem> implements MenuitemService {

}
