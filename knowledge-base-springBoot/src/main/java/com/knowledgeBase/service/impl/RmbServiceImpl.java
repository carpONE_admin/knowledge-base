package com.knowledgeBase.service.impl;

import com.knowledgeBase.entity.Rmb;
import com.knowledgeBase.mapper.RmbMapper;
import com.knowledgeBase.service.RmbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-05
 */
@Service
public class RmbServiceImpl extends ServiceImpl<RmbMapper, Rmb> implements RmbService {

}
