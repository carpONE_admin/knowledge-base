package com.knowledgeBase.service.impl;

import com.knowledgeBase.entity.House;
import com.knowledgeBase.mapper.HouseMapper;
import com.knowledgeBase.service.WaterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-11
 */
@Service
public class WaterServiceImpl extends ServiceImpl<HouseMapper, House> implements WaterService {

}
