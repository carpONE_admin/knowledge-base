package com.knowledgeBase.service;

import com.knowledgeBase.entity.Password;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 是否管理员 服务类
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-23
 */
public interface PasswordService extends IService<Password> {

}
