package com.knowledgeBase.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@TableName("t_menuitemgroupitem")
public class MenuitemGroupItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("MenuItemGroupItemId")
    private String MenuItemGroupItemId;

    @TableField("MenuItemGroupId")
    private String MenuItemGroupId;

    @TableField("MenuItemGroupItemTitle")
    private String MenuItemGroupItemTitle;

    @TableField("MenuItemGroupItemIcon")
    private String MenuItemGroupItemIcon;

    @TableField("createdTime")
    private LocalDateTime createdTime;

    private String menuItemGroupTitle;


}
