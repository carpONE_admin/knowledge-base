package com.knowledgeBase.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-11
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@TableName("t_house")
public class House implements Serializable {
    @TableId(value = "id")
    private String id;

    @TableField("type")
    private boolean type;

    private Integer count;

    private LocalDate day;

    @TableField("createTime")
    @DateTimeFormat(pattern = "YYYY-MM-DD HH:mm:ss")
    private LocalDateTime createTime;


}
