package com.knowledgeBase.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@TableName("t_content")
public class Content implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("contentId")
    private String contentId;

    @TableField("menuItemId")
    private String menuItemId;

    @TableField("contentDetail")
    private String contentDetail;

    @TableField("whereFrom")
    private String whereFrom;

    private String author;

    private String reference;

    @TableField("projectAddress")
    private String projectAddress;

    @TableField("createdTime")
    private LocalDateTime createdTime;

    @TableField("updateTime")
    private LocalDateTime updateTime;

    private String menuItemTitle;


}
