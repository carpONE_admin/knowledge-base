package com.knowledgeBase.entity;

import lombok.Data;

/**
 * @author liangyonghao
 * @date 2022年07月30日下午 10:44
 */
@Data
public class RmbVo {
    private String sumMoney;
    private String outTime;
}
