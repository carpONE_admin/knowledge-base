package com.knowledgeBase.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@TableName("t_menuitem")
public class Menuitem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("menuItemId")
    private String menuItemId;

    private String icon;

    @TableField("menuTitle")
    private String menuTitle;

    @TableField("isSubMenu")
    private Boolean isSubMenu;

    @TableField("hasMenuItemGroup")
    private Boolean hasMenuItemGroup;

    @TableField("createdTime")
    private String createdTime;


}
