package com.knowledgeBase.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 是否管理员
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_password")
public class Password implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("passwordKey")
    private String passwordKey;

    private Integer times;

    private Integer id;


}
