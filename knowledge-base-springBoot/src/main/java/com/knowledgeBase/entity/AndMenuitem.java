package com.knowledgeBase.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 鲤鱼乡
 * @date 2022年03月22日下午 8:42
 * @info AndMenuitem
 */
@Data
public class AndMenuitem implements Serializable {
    private String menuitem;
    private String menuTitle;
}
