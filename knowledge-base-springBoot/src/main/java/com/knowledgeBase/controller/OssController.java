package com.knowledgeBase.controller;

import cn.hutool.core.util.StrUtil;
import com.knowledgeBase.result.RestResponse;
import com.knowledgeBase.service.IOssService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 鲤鱼乡
 * date 2022年03月20日下午 2:56
 * info OssController
 */
@Controller
@RequestMapping("/ossController")
public class OssController {
    private final IOssService ossService;

    public OssController(IOssService ossService) {
        this.ossService = ossService;
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public RestResponse upload(MultipartFile file) {
        String url = StrUtil.toString(ossService.upload(file));
        if (url.equals("null")) return RestResponse.fail("图片类型不正确，可上传jpg、jpeg、png、gif、svg");
        return RestResponse.ok(url);
    }
}
