package com.knowledgeBase.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.knowledgeBase.entity.AndMenuitem;
import com.knowledgeBase.entity.MenuitemGroup;
import com.knowledgeBase.mapper.MenuitemgroupMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@RestController
@RequestMapping("/menuitemGroup")
public class MenuitemgroupController {
    private final MenuitemgroupMapper menuitemgroupMapper;

    public MenuitemgroupController(MenuitemgroupMapper menuitemgroupMapper) {
        this.menuitemgroupMapper = menuitemgroupMapper;
    }

    @GetMapping()
    public RestResponse queryAll() {
        List<MenuitemGroup> list = menuitemgroupMapper.selectAll();
        if (ObjectUtil.hasEmpty(list)) return RestResponse.fail("查无数据");
        return RestResponse.ok(list);
    }

    @GetMapping("byId")
    public RestResponse queryByMenuItemId(String id) {
        List<MenuitemGroup> list = menuitemgroupMapper.queryByMenuItemId(id);
        if (ObjectUtil.hasEmpty(list)) return RestResponse.ok("查无数据");
        return RestResponse.ok(list);
    }

    @GetMapping("i-t")
    public RestResponse queryIDAndTitle() {
        List<MenuitemGroup> groupList = menuitemgroupMapper.selectAll();
        if (ObjectUtil.hasEmpty(groupList)) return RestResponse.fail("查无数据");
        List<AndMenuitem> addList = new ArrayList();
        for (MenuitemGroup menuitemGroup : groupList) {
            AndMenuitem andMenuitem = new AndMenuitem();
            andMenuitem.setMenuitem(menuitemGroup.getMenuItemGroupId());
            andMenuitem.setMenuTitle(menuitemGroup.getMenuItemGroupTitle());
            addList.add(andMenuitem);
        }
        return RestResponse.ok(addList);
    }

    @PostMapping("add")
    public RestResponse addMenuItemGroup(@Validated @RequestBody MenuitemGroup menuitemGroup) {
        if (ObjectUtil.hasEmpty(menuitemGroup)) return RestResponse.fail("传入数据不正确");
        String menuItemGroupTitle = menuitemGroup.getMenuItemGroupTitle();

        String subMenuId = menuitemGroup.getSubMenuId();
        String subMenuTitle = menuitemGroup.getSubMenuTitle();
        if (StrUtil.hasEmpty(subMenuId) || StrUtil.hasEmpty(subMenuTitle) || StrUtil.hasEmpty(menuItemGroupTitle))
            return RestResponse.fail("subMenuId或者subMenuTitle或者标题不能为空");
        String menuItemGroupId = IdUtil.objectId();
        String createTime = DateUtil.now();
        boolean b = menuitemgroupMapper.addMenuitemgroup(menuItemGroupId, subMenuId, menuItemGroupTitle, menuitemGroup.getMenuItemGroupIcon(), createTime, subMenuTitle);
        if (b) return RestResponse.ok("添加成功");
        return RestResponse.fail("添加失败");
    }

    @PostMapping("update")
    public RestResponse updateMenuItemGroup(@Validated @RequestBody MenuitemGroup menuitemGroup) {
        if (ObjectUtil.hasEmpty(menuitemGroup)) return RestResponse.fail("传入数据不正确");
        String menuItemGroupId = menuitemGroup.getMenuItemGroupId();
        String subMenuId = menuitemGroup.getSubMenuId();
        String subMenuTitle = menuitemGroup.getSubMenuTitle();
        if (StrUtil.hasEmpty(menuItemGroupId) || StrUtil.hasEmpty(subMenuId) || StrUtil.hasEmpty(subMenuTitle))
            return RestResponse.fail("MenuItemGroupId或者subMenuId或者subMenuTitle不能为空");
        List<MenuitemGroup> groupList = menuitemgroupMapper.queryById(menuItemGroupId);
        if (ObjectUtil.hasEmpty(groupList)) return RestResponse.fail("查无此数据");
        boolean b = menuitemgroupMapper.updateMenuItemGroup(menuItemGroupId, subMenuId, menuitemGroup.getMenuItemGroupTitle()
                , menuitemGroup.getMenuItemGroupIcon(), subMenuTitle);
        if (b) return RestResponse.ok("修改成功");
        else return RestResponse.fail("修改失败");
    }

    @GetMapping("delete")
    public RestResponse deleteMenuItemGroup(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("menuItemId不能为空");
        List<MenuitemGroup> menuitemGroupList = menuitemgroupMapper.queryById(id);
        if (ObjectUtil.hasEmpty(menuitemGroupList)) return RestResponse.fail("查无数据");
        boolean b = menuitemgroupMapper.deleteById(id);
        if (b) return RestResponse.ok("删除成功");
        return RestResponse.fail("删除失败");
    }

}
