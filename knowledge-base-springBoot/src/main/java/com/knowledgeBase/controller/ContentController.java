package com.knowledgeBase.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.knowledgeBase.entity.Content;
import com.knowledgeBase.mapper.ContentMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@RestController
@RequestMapping("/content")
public class ContentController {

    private final ContentMapper contentMapper;


    public ContentController(ContentMapper contentMapper) {
        this.contentMapper = contentMapper;

    }

    @GetMapping()
    public RestResponse queryAll() {
        List<Content> list = contentMapper.selectAll();
        if (ObjectUtil.hasEmpty(list)) return RestResponse.fail("查无数据");
        return RestResponse.ok(list);
    }


    @GetMapping("queryId")
    public RestResponse queryByMenuItemId(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("contentId不能为空");
        Content list = contentMapper.selectByMenuItemId(id);
        if (ObjectUtil.hasEmpty(list)) return RestResponse.ok("没有数据");
        return RestResponse.ok(list);
    }

    @PostMapping("add")
    public RestResponse addContent(@Validated @RequestBody Content content) {
        if (BeanUtil.isEmpty(content)) return RestResponse.fail("传入数据不对");
        String articleId = IdUtil.objectId();
        String menuId = content.getMenuItemId();
        String menuTitle =content.getMenuItemTitle();
        String createDateTime = DateUtil.now();
        String contentDetail = content.getContentDetail();
        if (StrUtil.hasEmpty(menuId)||StrUtil.hasEmpty(menuTitle)) return RestResponse.fail("menuId或者menuTitle不能为空");
        if (StrUtil.hasEmpty(contentDetail)) return RestResponse.fail("内容不能为空");
        Content content1 = contentMapper.selectByMenuItemId(content.getMenuItemId());
        if (!ObjectUtil.hasEmpty(content1)) return RestResponse.fail("数据已存在");
        boolean isInsert = contentMapper.addContent(articleId, menuId,
                contentDetail, content.getWhereFrom(), content.getAuthor(),
                content.getReference(), content.getProjectAddress(), createDateTime,menuTitle);
        if (isInsert) return RestResponse.ok("添加成功");
        return RestResponse.fail("添加失败");
    }

    @PostMapping("update")
    public RestResponse updateContent(@Validated @RequestBody Content content) {
        if (BeanUtil.isEmpty(content)) return RestResponse.fail("传入数据不对");
        String contentId = content.getContentId();
        String menuId = content.getMenuItemId();
        String menuTitle = content.getMenuItemTitle();
        String time = DateUtil.now();
        if (StrUtil.hasEmpty(contentId)) return RestResponse.fail("内容id不能为空");
        if (StrUtil.hasEmpty(menuId) || StrUtil.hasEmpty(menuTitle)) return RestResponse.fail("menuId或者menuTitle不能为空");
        List<Content> contentList = contentMapper.selectByContentId(contentId);
        if (ObjectUtil.hasEmpty(contentList)) return RestResponse.fail("查无此数据");
        boolean b = contentMapper.updateContent(contentId, menuId, content.getContentDetail(),
                content.getWhereFrom(), content.getAuthor(), content.getReference(),
                content.getProjectAddress(), time, menuTitle);
        if (b)
            return RestResponse.ok("修改成功");
        else return RestResponse.fail("修改失败");
    }

    @GetMapping("delete")
    public RestResponse deleteContent(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("menuItemId为空");
        List<Content> content1 = contentMapper.selectByContentId(id);
        if (ObjectUtil.hasEmpty(content1)) return RestResponse.fail("数据不存在");
        boolean b = contentMapper.deleteContent(id);
        if (b) return RestResponse.ok("删除内容成功");
        return RestResponse.fail("删除内容失败");
    }

    @GetMapping("fuzzyQueryByTitle")
    public RestResponse fuzzyQueryByTitle(String title) {
        if (StrUtil.hasEmpty(title)) return RestResponse.fail("输入内容不能为空");
        List<Content> content = contentMapper.fuzzyQuery(title);
        if (ObjectUtil.hasEmpty(content)) return RestResponse.fail("查找数据为空");
        return RestResponse.ok(content);
    }

}
