package com.knowledgeBase.controller;


import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.knowledgeBase.entity.House;
import com.knowledgeBase.mapper.HouseMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-11
 */
@RestController
@RequestMapping("/house")
public class HouseController {

    private final HouseMapper houseMapper;

    public HouseController(HouseMapper houseMapper) {
        this.houseMapper = houseMapper;
    }

    @GetMapping()
    public RestResponse getList(boolean type, String starTime, String endTime) {
        List<House> list;
        if (StrUtil.hasEmpty(starTime) && StrUtil.hasEmpty(endTime))
            list = houseMapper.getForType(type);
        else
            list = houseMapper.getForTime(type, starTime, endTime);

        return RestResponse.ok(list);
    }

    @GetMapping("all")
    public RestResponse getAll() {
        HashMap hashMap = new HashMap();

        List<House> list1 = houseMapper.getOrder(true);
        List<House> list2 = houseMapper.getOrder(false);
        if (list1.size() != 0 && list1.size() != 0) {
            hashMap.put("electricity", list1.get(0));
            hashMap.put("water", list2.get(0));
            hashMap.put("eLast", list1.get(list1.size()-1));
            hashMap.put("wLast", list2.get(list2.size()-1));
        } else {
            House house = new House();
            house.setCount(0);
            hashMap.put("electricity", house);
            hashMap.put("water", house);
        }

        return RestResponse.ok(hashMap);
    }

    @GetMapping("del")
    public RestResponse delWater(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("存在空数据");
        try {
            int i = houseMapper.deleteById(id);
            if (i == 1)
                return RestResponse.ok("删除成功");
        } catch (Exception e) {
            return RestResponse.fail("删除失败");
        }
        return RestResponse.fail("删除失败");
    }

    @PostMapping("add")
    public RestResponse insertWater(@RequestBody House house) {
        house.setId(IdUtil.objectId());
        house.setCreateTime(LocalDateTime.now());
        try {
            int i = houseMapper.insert(house);
            if (i == 1)
                return RestResponse.ok("添加成功");
        } catch (Exception e) {
            return RestResponse.fail("添加失败");
        }
        return RestResponse.fail("添加失败");
    }

    @PostMapping("update")
    public RestResponse updateWater(@RequestBody House house) {
        if (StrUtil.hasEmpty(house.getId())) return RestResponse.fail("存在空数据");
        try {
            int i = houseMapper.update(house, new QueryWrapper<House>().eq("id", house.getId()));
            if (i == 1)
                return RestResponse.ok("更新成功");
        } catch (Exception e) {
            return RestResponse.fail("更新失败");
        }
        return RestResponse.fail("更新失败");
    }

}
