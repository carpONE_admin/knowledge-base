package com.knowledgeBase.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.knowledgeBase.entity.AndMenuitem;
import com.knowledgeBase.entity.Content;
import com.knowledgeBase.entity.Menuitem;
import com.knowledgeBase.entity.MenuitemGroupItem;
import com.knowledgeBase.mapper.ContentMapper;
import com.knowledgeBase.mapper.MenuitemMapper;
import com.knowledgeBase.mapper.MenuitemgroupitemMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 鲤鱼乡
 * date 2022年03月22日下午 2:54
 * info ContentAndMenuItem
 */
@RestController
public class ContentAndMenuItem {

    private final ContentMapper contentMapper;
    private final MenuitemMapper menuitemMapper;
    private final MenuitemgroupitemMapper menuitemgroupitemMapper;

    public ContentAndMenuItem(ContentMapper contentMapper, MenuitemMapper menuitemMapper, MenuitemgroupitemMapper menuitemgroupitemMapper) {
        this.contentMapper = contentMapper;
        this.menuitemMapper = menuitemMapper;
        this.menuitemgroupitemMapper = menuitemgroupitemMapper;
    }

    @GetMapping("c-m")
    public RestResponse QueryContentAndMenuItem() {
        List<Content> contentList = contentMapper.selectAll();
        List<Menuitem> menuitemList = menuitemMapper.selectList(new QueryWrapper<Menuitem>().eq("isSubMenu", false));
        List<MenuitemGroupItem> groupItemList = menuitemgroupitemMapper.selectAll();

        ArrayList<Object> contentMenuItemArray = new ArrayList<>();
        ArrayList<Object> addList = new ArrayList<>();

        for (Content content : contentList) {
            contentMenuItemArray.add(content.getMenuItemId());
        }

        for (Menuitem menuitem : menuitemList) {
            AndMenuitem andMenuitem = new AndMenuitem();
            if (!contentMenuItemArray.contains(menuitem.getMenuItemId())) {
                andMenuitem.setMenuitem(menuitem.getMenuItemId());
                andMenuitem.setMenuTitle(menuitem.getMenuTitle());
                addList.add(andMenuitem);
            }
        }
        for (MenuitemGroupItem menuitemGroupItem : groupItemList) {
            AndMenuitem andMenuitem = new AndMenuitem();
            if (!contentMenuItemArray.contains(menuitemGroupItem.getMenuItemGroupItemId())) {
                andMenuitem.setMenuitem(menuitemGroupItem.getMenuItemGroupItemId());
                andMenuitem.setMenuTitle(menuitemGroupItem.getMenuItemGroupItemTitle());
                addList.add(andMenuitem);
            }
        }


        return RestResponse.ok(addList);

    }
}
