package com.knowledgeBase.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.knowledgeBase.entity.AndMenuitem;
import com.knowledgeBase.entity.Menuitem;
import com.knowledgeBase.mapper.MenuitemMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@RestController
@RequestMapping("/menuitem")
public class MenuitemController {
    private final MenuitemMapper menuitemMapper;

    public MenuitemController(MenuitemMapper menuitemMapper) {
        this.menuitemMapper = menuitemMapper;
    }

    @GetMapping()
    public RestResponse queryAll() {
        List<Menuitem> list = menuitemMapper.selectAll();
        if (ObjectUtil.hasEmpty(list)) return RestResponse.fail("查无数据");
        return RestResponse.ok(list);
    }

    @GetMapping("submenu")
    public RestResponse queryIsSubmenu() {
        List<Menuitem> menuitemList = menuitemMapper.selectList(new QueryWrapper<Menuitem>().eq("isSubMenu", true));
        if (ObjectUtil.hasEmpty(menuitemList)) return RestResponse.fail("查无数据");
        ArrayList<AndMenuitem> addList = new ArrayList<>();
        for (Menuitem menuitem : menuitemList) {
            AndMenuitem andMenuitemArrayList = new AndMenuitem();
            andMenuitemArrayList.setMenuitem(menuitem.getMenuItemId());
            andMenuitemArrayList.setMenuTitle(menuitem.getMenuTitle());
            addList.add(andMenuitemArrayList);
        }

        return RestResponse.ok(addList);
    }

    @PostMapping("add")
    public RestResponse addMenuItem(@Validated @RequestBody Menuitem menuitem) {
        if (ObjectUtil.hasEmpty(menuitem)) return RestResponse.fail("传入数据不正确");
        String menuTitle = menuitem.getMenuTitle();
        if (StrUtil.hasEmpty(menuTitle)) return RestResponse.fail("标题不能为空");
        String menuItemId = IdUtil.objectId();
        String createTime = DateUtil.now();
        boolean b = menuitemMapper.addMenuItem(menuItemId, menuitem.getIcon(),
                menuTitle, menuitem.getIsSubMenu(), menuitem.getHasMenuItemGroup(), createTime);

        if (b) return RestResponse.ok("添加成功");
        return RestResponse.fail("添加失败");
    }

    @PostMapping("update")
    public RestResponse updateMenuItem(@Validated @RequestBody Menuitem menuitem) {
        if (ObjectUtil.hasEmpty(menuitem)) return RestResponse.fail("传入数据不正确");
        String menuId = menuitem.getMenuItemId();
        if (StrUtil.hasEmpty(menuId)) return RestResponse.fail("menuItem不能为空");
        List<Menuitem> menuitemList = menuitemMapper.queryById(menuId);
        if (ObjectUtil.hasEmpty(menuitemList)) return RestResponse.fail("查无此数据");
        boolean b = menuitemMapper.updateMenuItem(menuId, menuitem.getIcon(), menuitem.getMenuTitle(),
                menuitem.getIsSubMenu(), menuitem.getHasMenuItemGroup());

        if (b) return RestResponse.ok("修改成功");
        else return RestResponse.fail("修改失败");
    }

    @GetMapping("delete")
    public RestResponse deleteMenuItem(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("menuItemId不能为空");
        List<Menuitem> menuitemList = menuitemMapper.queryById(id);
        if (ObjectUtil.hasEmpty(menuitemList)) return RestResponse.fail("查无数据");
        boolean b = menuitemMapper.deleteById(id);
        if (b) return RestResponse.ok("删除成功");
        return RestResponse.fail("删除失败");
    }


}
