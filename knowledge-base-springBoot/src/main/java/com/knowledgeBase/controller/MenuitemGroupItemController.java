package com.knowledgeBase.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.knowledgeBase.entity.MenuitemGroupItem;
import com.knowledgeBase.mapper.MenuitemgroupitemMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-16
 */
@RestController
@RequestMapping("/menuitemGroupItem")
public class MenuitemGroupItemController {
    private final MenuitemgroupitemMapper menuitemgroupitemMapper;

    public MenuitemGroupItemController(MenuitemgroupitemMapper menuitemgroupitemMapper) {
        this.menuitemgroupitemMapper = menuitemgroupitemMapper;
    }

    @GetMapping()
    public RestResponse queryAll() {
        List<MenuitemGroupItem> list = menuitemgroupitemMapper.selectAll();
        if (ObjectUtil.hasEmpty(list)) return RestResponse.fail("查无数据");
        return RestResponse.ok(list);
    }

    @PostMapping("add")
    public RestResponse addMenuItemGroupItem(@Validated @RequestBody MenuitemGroupItem menuitemGroupItem) {
        if (ObjectUtil.hasEmpty(menuitemGroupItem)) return RestResponse.fail("传入数据不正确");
        String menuItemGroupItemTitle = menuitemGroupItem.getMenuItemGroupItemTitle();
        if (StrUtil.hasEmpty(menuItemGroupItemTitle)) return RestResponse.fail("标题不能为空");
        String groupId = menuitemGroupItem.getMenuItemGroupId();
        String groupTitle = menuitemGroupItem.getMenuItemGroupTitle();
        if (StrUtil.hasEmpty(groupId) || StrUtil.hasEmpty(groupTitle))
            return RestResponse.fail("groupId或者groupTitle不能为空");
        String menuItemGroupItemId = IdUtil.objectId();
        String createTime = DateUtil.now();
        boolean b = menuitemgroupitemMapper.addMenuitemgroupItem(menuItemGroupItemId, groupId,
                menuItemGroupItemTitle, menuitemGroupItem.getMenuItemGroupItemIcon(), createTime, groupTitle);
        if (b) return RestResponse.ok("添加成功");
        return RestResponse.fail("添加失败");
    }

    @PostMapping("update")
    public RestResponse updateMenuItemGroupItem(@Validated @RequestBody MenuitemGroupItem menuitemGroupItem) {
        if (ObjectUtil.hasEmpty(menuitemGroupItem)) return RestResponse.fail("传入数据不正确");
        String groupItemId = menuitemGroupItem.getMenuItemGroupItemId();
        String groupId = menuitemGroupItem.getMenuItemGroupId();
        String groupTitle = menuitemGroupItem.getMenuItemGroupTitle();
        if (StrUtil.hasEmpty(groupId) || StrUtil.hasEmpty(groupTitle))
            return RestResponse.fail("groupId或者groupTitle不能为空");
        List<MenuitemGroupItem> groupItemList = menuitemgroupitemMapper.queryById(groupItemId);
        if (ObjectUtil.hasEmpty(groupItemList)) return RestResponse.fail("查无此数据");
        boolean b = menuitemgroupitemMapper.updateMenuitemgroupItem(groupId,
                menuitemGroupItem.getMenuItemGroupItemTitle(), menuitemGroupItem.getMenuItemGroupItemIcon(), groupTitle, menuitemGroupItem.getMenuItemGroupItemId());
        if (b) return RestResponse.ok("修改成功");
        else return RestResponse.fail("修改失败");
    }

    @GetMapping("delete")
    public RestResponse deleteMenuItemGroupItem(String id) {
        if (StrUtil.hasEmpty(id)) return RestResponse.fail("menuItemId不能为空");
        List<MenuitemGroupItem> menuitemGroupItemList = menuitemgroupitemMapper.queryById(id);
        if (ObjectUtil.hasEmpty(menuitemGroupItemList)) return RestResponse.fail("查无数据");
        boolean b = menuitemgroupitemMapper.deleteById(id);
        if (b) return RestResponse.ok("删除成功");
        return RestResponse.fail("删除失败");
    }


}
