package com.knowledgeBase.controller;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.knowledgeBase.entity.Password;
import com.knowledgeBase.mapper.PasswordMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 是否管理员 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-03-23
 */
@RestController
@RequestMapping("/password")
public class PasswordController {
    private final PasswordMapper passwordMapper;

    public PasswordController(PasswordMapper passwordMapper) {
        this.passwordMapper = passwordMapper;
    }

    @PostMapping()
    @ResponseBody
    public RestResponse isAdministrator(@RequestBody String password) {
        if (StrUtil.hasEmpty(password)) return RestResponse.fail("输入有误，请重新输入");
        Password password1 = passwordMapper.selectById(1);
        if (password1.getTimes()>=3) return RestResponse.fail("已被锁定，请联系管理员");
        if (!DigestUtil.md5Hex(password).equals(password1.getPasswordKey())) {
            passwordMapper.updateForTimes(password1.getTimes() + 1, 1);
            return RestResponse.fail("密码不正确，请联系管理员");
        }
        else return RestResponse.ok(DigestUtil.md5Hex("12345678"));
    }
}
