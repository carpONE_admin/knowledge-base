package com.knowledgeBase.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.knowledgeBase.entity.Rmb;
import com.knowledgeBase.entity.RmbVo;
import com.knowledgeBase.mapper.RmbMapper;
import com.knowledgeBase.result.RestResponse;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 关注公众号：l鲤鱼乡
 * @since 2022-06-05
 */
@RestController
@RequestMapping("/rmb")
public class RmbController {
    private final RmbMapper rmbMapper;

    public RmbController(RmbMapper rmbMapper) {
        this.rmbMapper = rmbMapper;
    }

    @GetMapping("getForMonth")
    public RestResponse getAllExpenseForThisMonth(String type, String time1, String time2) {
        List<Rmb> rmbList;
        if (StrUtil.hasEmpty(type) && StrUtil.hasEmpty(time1) && StrUtil.hasEmpty(time2)) {
            rmbList = rmbMapper.getThisMonth();
        } else if (StrUtil.hasEmpty(time1) && StrUtil.hasEmpty(time2)) {
            rmbList = rmbMapper.getForType(type);
        } else {
            rmbList = rmbMapper.getTimePeriod(type, time1, time2);
        }

        return RestResponse.ok(rmbList);
    }

    @GetMapping("get")
    public RestResponse getList(String type, String time1, String time2) {
        if (StrUtil.hasEmpty(type)) return RestResponse.fail("类型为空");
        List<Rmb> rmbList;
        if (StrUtil.hasEmpty(time1) && StrUtil.hasEmpty(time2)) {
            rmbList = rmbMapper.selectList(new QueryWrapper<Rmb>().eq("type", type));
        } else {
            rmbList = rmbMapper.getTimePeriod(type, time1, time2);
        }

        return RestResponse.ok(rmbList);
    }

    @GetMapping("sum")
    public RestResponse getForSum(){
        List<RmbVo> rmbVoList = rmbMapper.getMoneySumForDate();
        return RestResponse.ok(rmbVoList);
    }

    @PostMapping("add")
    public RestResponse addList(@RequestBody Rmb rmb) {
        if (StrUtil.hasEmpty(rmb.getTitle()) || StrUtil.hasEmpty(rmb.getIncident()) || StrUtil.hasEmpty(rmb.getType()))
            return RestResponse.fail("不能存在为空数据");
        rmb.setExpenseId(IdUtil.objectId());
        rmb.setCreateTime((DateUtil.format(LocalDateTime.now(), "yyyy-MM-dd HH:mm")));
        Integer i = rmbMapper.insert(rmb);
        if (i.equals(0)) return RestResponse.fail("添加失败");
        return RestResponse.ok("添加成功");
    }

    @PostMapping("update")
    public RestResponse updateList(@RequestBody Rmb rmb) {
        Rmb rmb1 = rmbMapper.selectOne(new QueryWrapper<Rmb>().eq("expenseId", rmb.getExpenseId()));
        if (ObjectUtil.hasEmpty(rmb1)) return RestResponse.fail("更新失败--找不到");
        Integer i = rmbMapper.update(rmb, new QueryWrapper<Rmb>().eq("expenseId", rmb.getExpenseId()));
        if (i.equals(0)) return RestResponse.fail("更新失败--无法更新");
        return RestResponse.ok("更新成功");
    }

    @PostMapping("del")
    public RestResponse delRmb(@RequestBody Rmb rmb) {
        Integer i = rmbMapper.delete(new QueryWrapper<Rmb>()
                .eq("id", rmb.getId()).eq("expenseId", rmb.getExpenseId()));
        if (i.equals(0)) return RestResponse.fail("删除失败");
        return RestResponse.ok("删除成功");
    }

}
