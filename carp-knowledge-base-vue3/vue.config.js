const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    lintOnSave: true, //是否开启保存即检验
    transpileDependencies: true,
    // 基本路径
    publicPath: "/",
    // 输出目录
    outputDir: "dist",
    // 用于嵌套生成的静态资产（js,css，img，fonts）的目录
    assetsDir: "assets",
    // 生产环境SourceMap
    productionSourceMap: true,
    devServer: {
        port: 5022,
        // open: true,
        host: "0.0.0.0"
            // proxy: {} //设置代理
            /*   proxy: {
                  '/api': {
                      target: 'https://www.sogou.com', // 你请求的第三方接口
                      changeOrigin: true, // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
                      pathRewrite: { // 路径重写，
                          '^/api': '' // 替换target中的请求地址，也就是说/api=/target，请求target这个地址的时候直接写成/api即可。
                      }
                  },
              }, */
    },
    chainWebpack: config => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .tap(options => {
                options.compilerOptions = options.compilerOptions || {};
                options.compilerOptions.isCustomElement = tag => tag === 'iconpark-icon'
                    // modify the options...
                return options
            })
    }
})