import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//markdown
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import highlight from 'highlight.js'

//组件
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
//二次封装的axios 需要在页面异步调用才有数据
import axios from "@/util/axios";

//自个性scss和js
import '@/assets/sass/self.scss'
//echart
import 'echarts'
import ECharts from 'vue-echarts'

//路由导航守卫
import "@/util/route"


const app = createApp(App)
app.use(store).use(router).use(highlight).use(mavonEditor).use(Antd).mount('#app')
app.component('VChart', ECharts);

// http请求全局配置
app.config.globalProperties.$api = axios;

app.directive('highlight', (el) => {
    const blocks = el.querySelectorAll('pre code')
    blocks.forEach((block) => { highlight.highlightBlock(block) })
});

//流量器标题
router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title;
    }
    next()
});