import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [{
        path: '/',
        name: 'App',
        component: () =>
            import ('../App.vue'),
        redirect: { name: 'MainView' },
        children: [{
            path: '/',
            name: 'MainView',
            meta: {
                title: "鲤鱼知识库 | 首页"
            },
            redirect: { path: "/home/623acf7b639ab1b33452963e" },
            component: () =>
                import ('@/views/MainView.vue'),
            children: [{
                    path: 'home/:contentId',
                    name: 'HomeView',
                    component: () =>
                        import ('@/views/Home/HomeView.vue')
                },
                {
                    path: 'manage',
                    name: 'ManageBackend',
                    component: () =>
                        import ('@/views/ManageBackend/ManageBackend.vue')
                },
            ]
        }]
    },
    {
        path: '/life',
        name: 'life',
        meta: {
            title: '仪表盘'
        },
        redirect: { name: 'dashboard' },
        component: () =>
            import ('@/views/life/Life.vue'),
        children: [{
                path: 'dashboard',
                name: 'dashboard',
                meta: {
                    title: '仪表盘 | 数据'
                },
                component: () =>
                    import ('@/views/life/dashboard/Dashboard.vue'),
            },
            {
                path: '/work',
                name: 'work',
                meta: {
                    title: '工作'
                },
                redirect: { name: 'schedule' },
                component: () =>
                    import ('@/views/life/work/Work.vue'),
                children: [{
                        path: 'schedule',
                        name: 'schedule',
                        meta: {
                            title: '工作 | 日程'
                        },
                        component: () =>
                            import ('@/views/life/work/Schedule.vue'),
                    },
                    {
                        path: 'task',
                        name: 'task',
                        meta: {
                            title: '工作 | 任务'
                        },
                        component: () =>
                            import ('@/views/life/work/Task.vue'),
                    },
                    {
                        path: 'demand',
                        name: 'demand',
                        meta: {
                            title: '工作 | 需求'
                        },
                        component: () =>
                            import ('@/views/life/work/Demand.vue'),
                    },
                    {
                        path: 'daylyNewspaper',
                        name: 'daylyNewspaper',
                        meta: {
                            title: '工作 | 汇报 | 日报'
                        },
                        component: () =>
                            import ('@/views/life/work/report/DaylyNewspaper.vue'),
                    },
                    {
                        path: 'weeklyNewspaper',
                        name: 'weeklyNewspaper',
                        meta: {
                            title: '工作 | 周报'
                        },
                        component: () =>
                            import ('@/views/life/work/report/WeeklyNewspaper.vue'),
                    },
                    {
                        path: 'monthlyReport',
                        name: 'monthlyReport',
                        meta: {
                            title: '工作 | 月报'
                        },
                        component: () =>
                            import ('@/views/life/work/report/MonthlyReport.vue'),
                    },
                ]
            },

            {
                path: '/admin',
                name: 'admin',
                meta: {
                    title: '管理页面'
                },
                redirect: { name: 'rmb' },
                component: () =>
                    import ('@/views/life/admin/Admin.vue'),
                children: [{
                    path: 'rmb',
                    name: 'rmb',
                    meta: {
                        title: '管理页面 | 支出收入管理'
                    },
                    component: () =>
                        import ('@/views/life/admin/rmb/Rmb.vue'),
                }, {
                    path: 'house/:type',
                    name: 'house',
                    meta: {
                        title: '管理页面 | 租房管理'
                    },
                    component: () =>
                        import ('@/views/life/admin/houseExpense/House.vue'),
                }, ]


            }
        ]
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router