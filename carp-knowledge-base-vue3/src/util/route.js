import router from '../router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
NProgress.configure({
    easing: 'ease', // 动画方式
    speed: 500, // 递增进度条的速度
    showSpinner: true, // 是否显示加载ico
    trickleSpeed: 200, // 自动递增间隔
    minimum: 0.3 // 初始化时的最小百分比
})
const lifeMenu = localStorage.getItem("lifeMenu")
const admin = 'admin'
const life = 'life'
const work = 'work'
router.beforeEach((to, from) => {
    NProgress.start() // start progress bar
    const list = to.path.split("/")
    if (list[1] === life) {
        localStorage.setItem("lifeMenu", to.name)
    } else if (list[1] === work) {
        localStorage.setItem("lifeMenu", list[1])
        localStorage.setItem("workMenu", to.name)
    } else {
        localStorage.setItem("lifeMenu", list[1])
    }
})

router.afterEach(() => {
    NProgress.done() // finish progress bar
})