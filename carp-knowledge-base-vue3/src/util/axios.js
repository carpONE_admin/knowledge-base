import axios from 'axios'
import { message } from 'ant-design-vue'
axios.defaults.headers.post['Content-Type'] = 'application/json'

/* 设置超时 */
axios.defaults.timeout = 10000
axios.defaults.baseURL = "http://120.76.136.48:5000/"
axios.interceptors.request.use(
    (config) => { return config },
    (error) => { return Promise.reject(error) })

axios.interceptors.response.use(
    (response) => {
        if (response.status === 200) { return Promise.resolve(response) } else { return Promise.resolve(response) }
    },
    (error) => {
        if (error.response.data) { error.message = error.response.data.msg }
        if (error.response.status !== 200) { message.error("服务异常") }
    })
export default {
    post(url, data) {
        return new Promise((resolve, reject) => {
            axios({
                    method: 'POST',
                    headers: { authenticate: 'Access-Control-Allow-Origin' },
                    url: url,
                    data: (data)
                }).then((res) => { resolve(res.data) })
                .catch((err) => { message.error("服务异常") })
        })
    },

    get(url, data) {
        return new Promise((resolve, reject) => {
            axios({
                    method: 'GET',
                    headers: { authenticate: 'Access-Control-Allow-Origin' },
                    url: url,
                    params: data
                }).then((res) => { resolve(res.data) })
                .catch(() => { message.error("服务异常") })
        })
    }
}