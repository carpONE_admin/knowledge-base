# VUE3 的使用

- 全局使用 1

```js
// 要进行同步操作
import { getCurrentInstance, onMounted } from "vue";

onMounted(async () => {
  const {
    appContext: {
      config: { globalProperties },
    },
  } = getCurrentInstance();
  menuData.menuItem = await globalProperties.$get("menuitem");
});
```

- 全局使用 2

```js
import { getCurrentInstance } from "vue";
export default defineComponent({
  setup() {
    const { proxy } = getCurrentInstance();
  },
  getInfo() {
    proxy.$api;
  },
});
```

- 问题 1

```js
//全局中添加这个正则表达式
app.config.globalProperties.$regularPatternPass = /^0$/g;
app.config.globalProperties.$regularPatternNo = /^([1-9]|1[0-9])$/g;

//在其他页面上进行使用中只能用一次否则第二次或者第三次使用无效
if (proxy.$regularPatternPass.test(res.error)) console.log("正确");
if (proxy.$regularPatternNo.test(res.error)) message.error(res.message);
console.log("错误");
```

- 知识要点 1(计算属性传参)

```js
const cMenuItemGroupItem = computed(() => {
  return function (id) {
    return menuData.menuItemGroupItem.filter(
      (item) => id === item.menuItemGroupId
    );
  };
});
```

- 知识要点 2(在 vue 中创建 script 标签)

```js
/*  created() {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src =
      "https://lf1-cdn-tos.bytegoofy.com/obj/iconpark/icons_7102_13.625adc0d3ef41d66b493f2e567d3e7c9.es5.js";
    document.getElementsByTagName("head")[0].appendChild(script);
  }, */
```

## 1、vue3 使用 Echart

> 注意：宽度自适应这种效果在一个页面只存在一个图表生效

- 组件化使用

> 首先 npm install echarts
> 然后在 main.js 中
>
> ```js
> import { createApp } from "vue";
> import App from "./App.vue";
> import * as echarts from "echarts";
> const app = createApp(App);
> //echart图表全局配置
> app.config.globalProperties.$echarts = echarts;
> ```
>
> .

- 示例

```html
<template>
  <div>
    <div ref="echartsRef" id="main" style="height: 400px"></div>
  </div>
</template>

<script>
  import {
    defineComponent,
    getCurrentInstance,
    onMounted,
    onBeforeUnmount,
    ref,
  } from "vue";
  import _ from "lodash"; //引入lodash使用防抖功能
  export default defineComponent({
    props: { options: Object }, //父传子数据
    setup(props) {
      const { proxy } = getCurrentInstance(); //获取全局变量
      let echartsRef = ref(""); //挂载dom

      onMounted(() => {
        const myChart = proxy.$echarts.init(echartsRef.value); //初始化图表
        myChart.setOption(props.options); //应用
        window.onresize = _.debounce(() => {
          //宽度自适应
          myChart.resize();
        }, 0);
      });
      onBeforeUnmount(() => {
        window.onresize = null;
      });
      return {
        echartsRef,
      };
    },
  });
</script>
```

## 2、vue3 使用 Echart

> 优点：解决在一个页面中存在多个图表只有一个生效宽度自适应

> 首先 npm install echarts vue-echarts
> 然后在 main.js 中
>
> ```js
> import { createApp } from "vue";
> import App from "./App.vue";
> import "echarts";
> import ECharts from "vue-echarts";
> const app = createApp(App);
> //echart图表全局配置
> app.component("VChart", ECharts);
> ```
>
> .

- 示例

```html
<template>
  <div>
    <v-chart autoresize :option="option" style="height: 400px"></v-chart>
  </div>
</template>
<script>
  import { defineComponent } from "vue";
  export default defineComponent({
    setup() {
      const option = ref({
        title: {
          text: "租房支出列表",
          subtext: "5￥/方,1.5￥/度",
          left: "center",
        },
        tooltip: {},
        legend: {
          orient: "vertical",
          left: "left",
        },
        series: [
          {
            name: "支出列表",
            type: "pie",
            radius: [0, "50%"],
            data: [
              { value: 5, name: "水费" },
              { value: 10.5, name: "电费" },
              { value: 1, name: "用水量(方)" },
              { value: 9, name: "用电量(度)" },
            ],
          },
        ],
      });
      return { option };
    },
  });
</script>
```
