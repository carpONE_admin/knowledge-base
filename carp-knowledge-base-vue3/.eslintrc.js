module.exports = {
    root: false,
    extends: [
        'plugin:vue/vue3-essential',
        '@vue/standard'
    ],
    parserOptions: {
        parser: '@babel/eslint-parser'
    },
    env: {
        // 预定义的全局变量，这里是浏览器环境
        browser: true,
        node: true,
        es6: true
    },
    //0为去除 1为警告 2为报错
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        // 禁止或强制在单行代码块中使用空格
        'block-spacing': [2, 'always'],
        //取消最后该规则的校验
        'eol-last': 0,
        indent: [0, 8],
        "space-before-function-paren": 1, //括号前有空格1为警告
        "comma-dangle": 1, //意想不到的后面的逗号
        semi: 0, //尾部是否有多余分号
        quotes: 0, //双引号
        "spaced-comment": 0, //注释为//
        "no-multiple-empty-lines": 0, //是否可用超过两行空行
        curly: 0, //检验if语句后面是否有{}
        "func-call-spacing": 0, //函数名和括号之间有意外空格
        "vue/multi-word-component-names": 1
    }
}