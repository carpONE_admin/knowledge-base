-- MySQL dump 10.13  Distrib 5.5.62, for linux-glibc2.12 (x86_64)
--
CREATE TABLE `t_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentId` varchar(25) NOT NULL,
  `menuItemId` varchar(25) NOT NULL,
  `contentDetail` longblob NOT NULL,
  `whereFrom` varchar(50) DEFAULT NULL,
  `author` varchar(25) DEFAULT '鲤鱼',
  `reference` longblob,
  `projectAddress` longblob,
  `createdTime` datetime NOT NULL,
  `updateTime` datetime DEFAULT NULL,
  `menuItemTitle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;


CREATE TABLE `t_menuitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuItemId` varchar(25) NOT NULL,
  `icon` varchar(15) DEFAULT 'jinliyu',
  `menuTitle` varchar(25) NOT NULL,
  `isSubMenu` tinyint(1) DEFAULT '0',
  `hasMenuItemGroup` tinyint(1) DEFAULT '0',
  `createdTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



CREATE TABLE `t_menuitemgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MenuItemGroupId` varchar(25) NOT NULL,
  `subMenuId` varchar(25) NOT NULL,
  `MenuItemGroupTitle` varchar(25) NOT NULL,
  `MenuItemGroupIcon` varchar(15) DEFAULT 'jinliyu',
  `createdTime` datetime NOT NULL,
  `subMenuTitle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;



CREATE TABLE `t_menuitemgroupitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MenuItemGroupItemId` varchar(25) NOT NULL,
  `MenuItemGroupId` varchar(25) NOT NULL,
  `MenuItemGroupItemTitle` varchar(25) NOT NULL,
  `MenuItemGroupItemIcon` varchar(15) DEFAULT 'jinliyu',
  `createdTime` datetime NOT NULL,
  `menuItemGroupTitle` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;



CREATE TABLE `t_password` (
  `passwordKey` varchar(255) NOT NULL,
  `times` int(11) DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='是否管理员';


INSERT INTO `t_password` VALUES ('a5f695f41dcb99515baba30bff09a80d',3,1);