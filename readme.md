设计灵感来源：[javaguide 知识库](http://javaguide.cn/home.html#%E5%BF%85%E7%9C%8B%E4%B8%93%E6%A0%8F)

　　设计初稿：工具--> [即时工具](https://js.design/f/__7Ud_?p=bzg2dKZsfT)

　　设计初心：拥有自己的技术博客网页，创造自己的笔记中心、知识存储库。

　　

　　![知识库知识栈.png](./知识库知识栈.png)

　　

### 侧边栏数据库设计

* 根据代码逻辑设计：

```html
  <a-menu-item key="home">
            <template #icon>
              <iconpark-icon name="jinliyu"></iconpark-icon>
            </template>
            知识库必读
</a-menu-item>
<a-sub-menu key="sub1">
            <template #icon>
              <iconpark-icon name="css"></iconpark-icon>
            </template>
            <template #title>css</template>
            <a-menu-item-group key="g1">
              <template #title>
                <iconpark-icon name="css" class="mr-1"></iconpark-icon>Item 1
              </template>
              <a-menu-item key="1">
                <template #icon> <iconpark-icon name="css"></iconpark-icon> </template>
                css
              </a-menu-item>
              <a-menu-item key="2">Option 2</a-menu-item>
            </a-menu-item-group>
</a-sub-menu>
```

　　

　　![数据库.png](./数据库.png)

　　

## SpringBoot 后端

* 工具：IntelliJ IDEA 2020.3.1 x64
* 系统：Windows x64
* 项目 java 版本：jdk1.8
* spring boot 版本 ：2.2.6.RELEASE

#### 1、新建 spring boot 项目

1. 选择依赖：Spring Boot DevTools、Lombok、Spring Configuration Processor、Spring Web、MySQL Driver

　　

### 2、添加依赖

> 如无则添，如有则忽略
>

```xml
 <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-freemarker</artifactId>
</dependency>

 <!-- 实体类需求-->
 <dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>

 <!--mp代码生成器-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.2.0</version>
</dependency>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.2.0</version>
</dependency>

 <!-- hutool工具类-->
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.3.3</version>
</dependency>
```

　　

　　

### 3、添加配置文件

> 在目录 src/main/resources 中
>

　　添加名为：application 的 yml 文件

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/***?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai #数据库配置
    username: *** #数据库用户名
    password: *** #数据库密码
mybatis-plus:
  mapper-locations: classpath*:/mapper/**Mapper.xml
server:
  port: 5000 #启动端口
```

　　

### 4、添加必须文件

> 在目录 src/main/java/包名 中新建 util 包
>

* 根据数据库表自动生成器
  ```java
  public class CodeGeneration {

      /**
       * <p>
       * 读取控制台内容
       * </p>
       */
      public static String scanner(String tip) {
          Scanner scanner = new Scanner(System.in);
          System.out.println("请输入" + tip + "：");
          if (scanner.hasNext()) {
              String ipt = scanner.next();
              if (StringUtils.isNotEmpty(ipt)) {
                  return ipt;
              }
          }
          throw new MybatisPlusException("请输入正确的" + tip + "！");
      }

      public static void main(String[] args) {
          // 代码生成器
          AutoGenerator mpg = new AutoGenerator();

          // 全局配置
          GlobalConfig gc = new GlobalConfig();
          String projectPath = System.getProperty("user.dir");
          gc.setOutputDir(projectPath + "/src/main/java");
          gc.setAuthor("关注公众号：l鲤鱼乡"); //选择性配置
          gc.setOpen(false);
          // gc.setSwagger2(true); 实体属性 Swagger2 注解
          gc.setServiceName("%sService");
          mpg.setGlobalConfig(gc);

          // 数据源配置
          DataSourceConfig dsc = new DataSourceConfig();
          dsc.setUrl("jdbc:mysql://localhost:3306/***?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=UTC");
          dsc.setDriverName("com.mysql.cj.jdbc.Driver");
          dsc.setUsername("***");
          dsc.setPassword("***");
          mpg.setDataSource(dsc);

          // 包配置
          PackageConfig pc = new PackageConfig();
          pc.setModuleName(null);
          pc.setParent("com.knowledgeBase");//包名按需更改
          mpg.setPackageInfo(pc);

          // 自定义配置
          InjectionConfig cfg = new InjectionConfig() {
              @Override
              public void initMap() {}
          };

          // 如果模板引擎是 freemarker
          String templatePath = "/templates/mapper.xml.ftl";
          // 如果模板引擎是 velocity
          // String templatePath = "/templates/mapper.xml.vm";

          // 自定义输出配置
          List<FileOutConfig> focList = new ArrayList<>();
          // 自定义配置会被优先输出
          focList.add(new FileOutConfig(templatePath) {
              @Override
              public String outputFile(TableInfo tableInfo) {
                  // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                  return projectPath + "/src/main/resources/mapper/"
                          + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
              }
          });

          cfg.setFileOutConfigList(focList);
          mpg.setCfg(cfg);

          // 配置模板
          TemplateConfig templateConfig = new TemplateConfig();

          templateConfig.setXml(null);
          mpg.setTemplate(templateConfig);

          // 策略配置
          StrategyConfig strategy = new StrategyConfig();
          strategy.setNaming(NamingStrategy.underline_to_camel);
          strategy.setColumnNaming(NamingStrategy.underline_to_camel);
          strategy.setEntityLombokModel(true);
          strategy.setRestControllerStyle(true);
          strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
          strategy.setControllerMappingHyphenStyle(true);
          strategy.setTablePrefix("t_");//按照表名t_切割
          mpg.setStrategy(strategy);
          mpg.setTemplateEngine(new FreemarkerTemplateEngine());
          mpg.execute();
      }
  }

  ```
* MybatisPlus 配置文件
  ```java
  @Configuration
  @EnableTransactionManagement
  @MapperScan("com.knowledgeBase.mapper") //mapper包所在目录
  public class MybatisPlusConfig {
      @Bean
      public PaginationInterceptor paginationInterceptor() {
          PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
          return paginationInterceptor;
      }
  }
  ```

* 解决跨域问题

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }
}
```


### 使用

克隆项目：git clone https://gitee.com/lyxalyh/knowledge-base.git

也可以到https://gitee.com/lyxalyh/knowledge-base.git下载zip包

#### 前端vue使用

> 环境需要：@vue/cli(建议最新版) 、node(尽量14版本)

进入 carp-knowledge-base-vue3 目录

```bash
npm install     (安装依赖)
npm run serve   (运行)
```
运行后在浏览器中输入：localhost:5022 即可

#### 后端使用

> 环境需要：IDEA

1. 修改配置文件

> 修改 knowledge-base-springBoot/src/main/resources 中的application.yml

```yml
 url: jdbc:mysql://localhost:3306/knowledge_base? #按自己数据名称修改
    username: knowledge_base  #修改数据库用户名
    password: knowledge_base1 #修改数据库密码
aliyun:
  oss:
    # oss对外服务的访问域名-----修改自己阿里云对象存储地址
    endpoint: oss-cn-zhangjiakou.aliyuncs.com
    # 访问身份验证中用到用户标识------修改为自己阿里云的访问密钥
    accessKeyId: *
    # 用户用于加密签名字符串和oss用来验证签名字符串的密钥
    accessKeySecret: *
    # oss的存储空间
    bucketName: lyh-liyu
    # 上传文件大小(M)
    maxSize: 10
    # 上传文件夹路径前缀
    dir:
      prefix: carpKnowledgeBase/
```